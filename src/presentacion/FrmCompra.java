package presentacion;

import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import logica.Compra;
import logica.CompraDetalle;
import logica.Tipo_comprobante;
import logica.configuracion;
import util.Funciones;

public class FrmCompra extends javax.swing.JDialog {

    public int accion = 0;// si agrega o no agrega.

    public FrmCompra(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        //Asignar el modo de selección y el alto a cada fila del jtab
        tblCompraDetalle.setCellSelectionEnabled(true);
        tblCompraDetalle.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblCompraDetalle.setRowHeight(25);
        //Llamar aqui al método para obtener la tasa de IGV
        this.obtenerTasaIGV();

        //Llamar al método para llenar el combo tipo de comprobante
        this.llenarComboTC();

        //Llamar la método que permita configurar la cabecera de la tabla
        this.configurarCabecera();
    }

    private void configurarCabecera() {
        try {
            ResultSet resultado = new CompraDetalle().configurarTablaDetalle();
            String alineacion[] = {"C", "I", "D", "D", "D", "D"};
            int ancho[] = {70, 350, 100, 100, 110, 100};
            Funciones.llenarTabla(tblCompraDetalle, resultado, ancho, alineacion);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    private void llenarComboTC() {
        try {
            new Tipo_comprobante().llenarComboTC(cboTC);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    private void obtenerTasaIGV() {
        try {
            this.txtPorIGV.setText(new configuracion().obtenerValorConfiguracion(1));
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    private void calcularTotales() {
        try {
            double tasaIgv = 0;
            if (this.txtPorIGV.getText().isEmpty()) {
                tasaIgv = 0;
            }
            tasaIgv = Double.parseDouble(this.txtPorIGV.getText());
            double totalNeto = 0;
            for (int i = 0; i < this.tblCompraDetalle.getRowCount(); i++) {//Count numero de filas
                double importe = Double.parseDouble(this.tblCompraDetalle.getValueAt(i, 5).toString().replace(",", ""));
                totalNeto = totalNeto + importe;
            }

            double subTotal = 0;
            double igv = 0;
            subTotal = totalNeto / (1 + (tasaIgv / 100));
            igv = totalNeto - subTotal;

            //Mostrar los valores en los controles
            this.lblSubTotal.setText(Funciones.formatearNumero(subTotal));
            this.lblIGV.setText(Funciones.formatearNumero(igv));
            this.lblNeto.setText(Funciones.formatearNumero(totalNeto));
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtNroCompra = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cboTC = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNroSerie = new javax.swing.JTextField();
        txtNroDoc = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtNroFecha = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        txtPorIGV = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtRUC = new javax.swing.JTextField();
        lblRazónSocial = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCompraDetalle = new javax.swing.JTable();
        btnAgregar = new javax.swing.JButton();
        btnQuitar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        lblTelefono = new javax.swing.JLabel();
        btngrabar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        lblSubTotal = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lblIGV = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        lblNeto = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("N° Compra");

        txtNroCompra.setEditable(false);
        txtNroCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNroCompraActionPerformed(evt);
            }
        });

        jLabel2.setText("Tipo Comprobante");

        cboTC.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel3.setText("N° Documento");

        jLabel4.setText("N° Serie");

        jLabel5.setText("Fecha Compra");

        jLabel6.setText("IGV(%)");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Personales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 0, 204))); // NOI18N

        jLabel7.setText("RUC:");

        jLabel8.setText("Razón Social:");

        jLabel9.setText("Dirección:");

        txtRUC.setBackground(new java.awt.Color(240, 240, 240));
        txtRUC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRUCActionPerformed(evt);
            }
        });

        jLabel12.setText("Telefono:");

        jLabel14.setForeground(new java.awt.Color(0, 51, 255));
        jLabel14.setText("Artículos registrados en la compra:");

        tblCompraDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCompraDetalle.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tblCompraDetallePropertyChange(evt);
            }
        });
        tblCompraDetalle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblCompraDetalleKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblCompraDetalle);

        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add2.png"))); // NOI18N
        btnAgregar.setText("Agregar artículo");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnQuitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/delete.png"))); // NOI18N
        btnQuitar.setText("Quitar artículo");
        btnQuitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarActionPerformed(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cargos.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtRUC, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscar))
                            .addComponent(lblRazónSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 560, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 392, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAgregar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnQuitar, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 626, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(25, 25, 25))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtRUC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(lblRazónSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(lblDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(lblTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(btnAgregar)
                    .addComponent(btnQuitar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btngrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        btngrabar.setText("Grabar la compra");
        btngrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngrabarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jLabel15.setText("Sub. Total:");

        lblSubTotal.setText("0.00");

        jLabel17.setText("IGV:");

        lblIGV.setText("0.00");

        jLabel19.setText("Neto:");

        lblNeto.setText("0.00");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblIGV)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblNeto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(160, 160, 160))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSubTotal)
                    .addComponent(jLabel17)
                    .addComponent(lblIGV)
                    .addComponent(jLabel19)
                    .addComponent(lblNeto))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(58, 58, 58)
                                .addComponent(jLabel2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNroCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cboTC, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(38, 38, 38)
                                .addComponent(jLabel3)
                                .addGap(36, 36, 36)
                                .addComponent(jLabel5)
                                .addGap(51, 51, 51)
                                .addComponent(jLabel6))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNroSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtNroDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtNroFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtPorIGV, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btngrabar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSalir)
                        .addGap(27, 27, 27)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 651, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtNroCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cboTC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtNroSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtNroDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtNroFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPorIGV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btngrabar)
                            .addComponent(btnSalir))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtRUCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRUCActionPerformed

    }//GEN-LAST:event_txtRUCActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        FrmProveedorBuscar objFrm = new FrmProveedorBuscar(null, true);// represeta al contructor parametrizado(Por que estamos mandando valores)
        objFrm.setVisible(true);

        if (objFrm.accion == 1) {//hizo clic en el boton aceptar
            //Colocar aqui las instrucciones para capturar los datos retornados
            int filaSeleccionada = objFrm.tblResultadp.getSelectedRow();

            //Capturar los datos del Jtable en funcion a la fila seleccionada
            String ruc = objFrm.tblResultadp.getValueAt(filaSeleccionada, 0).toString();
            String rz = objFrm.tblResultadp.getValueAt(filaSeleccionada, 1).toString();
            String dir = objFrm.tblResultadp.getValueAt(filaSeleccionada, 2).toString();
            String tel = objFrm.tblResultadp.getValueAt(filaSeleccionada, 3).toString();

            this.txtRUC.setText(ruc);
            this.lblRazónSocial.setText(rz);
            this.lblDireccion.setText(dir);
            this.lblTelefono.setText(tel);
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        FrmArticuloBuscar objFrm = new FrmArticuloBuscar(null, true);// represeta al contructor parametrizado(Por que estamos mandando valores)
        objFrm.setVisible(true);

        if (objFrm.accion == 1) {//hizo clic en el boton aceptar
            //Colocar aqui las instrucciones para capturar los datos retornados
            int filaSeleccionada = objFrm.tblResultadp.getSelectedRow();

            Object registro[] = new Object[6];
            registro[0] = objFrm.tblResultadp.getValueAt(filaSeleccionada, 0);
            registro[1] = objFrm.tblResultadp.getValueAt(filaSeleccionada, 1);
            registro[2] = 0;
            registro[3] = 0;
            registro[4] = 0;
            registro[5] = 0;

            DefaultTableModel modelo = (DefaultTableModel) this.tblCompraDetalle.getModel();
            modelo.addRow(registro);

            //Enfocar a la celda cantidad
            this.tblCompraDetalle.changeSelection(this.tblCompraDetalle.getRowCount() - 1, 2, false, false);
            this.tblCompraDetalle.requestFocus();
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void tblCompraDetallePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tblCompraDetallePropertyChange
        if (evt.getPropertyName().equalsIgnoreCase("tableCellEditor")) {
            int filaEditada = this.tblCompraDetalle.getEditingRow();
            int Columnaeditada = this.tblCompraDetalle.getEditingColumn();

            if (Columnaeditada == 2 || Columnaeditada == 3 || Columnaeditada == 4) {
                int cantidad = Integer.parseInt(this.tblCompraDetalle.getValueAt(filaEditada, 2).toString());
                double precio = Double.parseDouble(this.tblCompraDetalle.getValueAt(filaEditada, 3).toString());
                double descuento = Double.parseDouble(this.tblCompraDetalle.getValueAt(filaEditada, 4).toString());
                double importe = new CompraDetalle().calcularImporte(cantidad, precio, descuento);
                this.tblCompraDetalle.setValueAt(Funciones.formatearNumero(importe), filaEditada, 5);
                //Llamar al método para calcular los totales
                this.calcularTotales();
            }
        }
    }//GEN-LAST:event_tblCompraDetallePropertyChange

    private void btnQuitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarActionPerformed
        DefaultTableModel tablaDetalle = (DefaultTableModel) this.tblCompraDetalle.getModel();

        int fila = this.tblCompraDetalle.getSelectedRow();

        if (fila < 0) {
            Funciones.mensajeError("Debe seleccionar", "verificar");
            return;
        }

        String articulo = tblCompraDetalle.getValueAt(fila, 1).toString();
        int respuesta = Funciones.mensajeConfirmacion("Está seguro de quitar el articulo: " + articulo, "Conforme");

        if (respuesta != 0) {
            return;
        }
        tablaDetalle.removeRow(fila);
        this.tblCompraDetalle.setModel(tablaDetalle);
    }//GEN-LAST:event_btnQuitarActionPerformed

    private void tblCompraDetalleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblCompraDetalleKeyPressed
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_DELETE:
                btnQuitar.doClick();
                break;

            case KeyEvent.VK_INSERT:
                btnAgregar.doClick();
                break;
        }
    }//GEN-LAST:event_tblCompraDetalleKeyPressed

    private void btngrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngrabarActionPerformed
        try {
            int respuesta = Funciones.mensajeConfirmacion("Desea grabar la compra", "Confirme");

            if (respuesta == 1) {
                return;//El usuario hizo el clic en el botón no
            }

            String codigoTipoComprobante;
            String rucProveedor;
            int numeroSerie;
            int numeroDocumento;
            java.sql.Date fechaCompra;
            double porcentajeIgv;
            double subTotal;
            double igv;
            double total;
            int codigoUsuario;

            codigoTipoComprobante = Tipo_comprobante.listarTC.get(this.cboTC.getSelectedIndex()).getCodigoTC();
            rucProveedor = this.txtRUC.getText();
            numeroSerie = Integer.parseInt(this.txtNroSerie.getText());
            numeroDocumento = Integer.parseInt(this.txtNroDoc.getText());
            fechaCompra = new java.sql.Date(this.txtNroFecha.getDate().getTime());
            porcentajeIgv = Double.parseDouble(this.txtPorIGV.getText());
            subTotal = Double.parseDouble(this.lblSubTotal.getText().replace(";", ""));
            igv = Double.parseDouble(this.lblIGV.getText().replace(",", ""));
            total = Double.parseDouble(this.lblNeto.getText().replace(",", ""));
            codigoUsuario = Funciones.CODIGO_USUARIO_INICIO_SESION;

            /*Crear el objeto objCompra para enviar los datos*/
            Compra objCompra = new Compra();
            objCompra.setCodigoTipoComprobante(codigoTipoComprobante);
            objCompra.setRucProveedor(rucProveedor);
            objCompra.setNumeroSerie(numeroSerie);
            objCompra.setNumeroDocumento(numeroDocumento);
            objCompra.setFechaCompra(fechaCompra);
            objCompra.setPorcentajeIGV(porcentajeIgv);
            objCompra.setSubTotal(subTotal);
            objCompra.setIgv(igv);
            objCompra.setTotal(total);
            objCompra.setCodigoUsuario(codigoUsuario);
            /*Captura los datos del jtable para el detalle de la compra*/
            ArrayList<CompraDetalle> detalleompra = new ArrayList<CompraDetalle>();
            /*Reorrercada fila de la tabla JTable*/
            for (int i = 0; i < tblCompraDetalle.getRowCount(); i++) {
                int codigo_articulo = Integer.parseInt(this.tblCompraDetalle.getValueAt(i, 0).toString());
                int cantidad = Integer.parseInt(this.tblCompraDetalle.getValueAt(i, 2).toString());
                double precio = Double.parseDouble(this.tblCompraDetalle.getValueAt(i, 3).toString());
                double descuento = Double.parseDouble(this.tblCompraDetalle.getValueAt(i, 3).toString());
                double importe = Double.parseDouble(this.tblCompraDetalle.getValueAt(i, 5).toString().replace(",", ""));

                CompraDetalle objComDet = new CompraDetalle();
                objComDet.setCodigo_articulo(codigo_articulo);
                objComDet.setCantidad(cantidad);
                objComDet.setPrecio(precio);
                objComDet.setDescuento(descuento);
                objComDet.setImporte(importe);

                detalleompra.add(objComDet);

                objCompra.setDetalleCompra(detalleompra);
                int retorno = objCompra.registrarCompra();
                if (retorno == 1) {
                    Funciones.mensajeInformacion("La compra registró con éxito", "Compra");
                    this.accion = 1;//Grabó ok
                    this.dispose();
                }
            }
        } catch (Exception w) {
            Funciones.mensajeError(w.getMessage(), "Error");
        }
    }//GEN-LAST:event_btngrabarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.accion = 0;
        this.dispose();// TODO add your handling code here:
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtNroCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNroCompraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNroCompraActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnQuitar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btngrabar;
    private javax.swing.JComboBox cboTC;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblIGV;
    private javax.swing.JLabel lblNeto;
    private javax.swing.JLabel lblRazónSocial;
    private javax.swing.JLabel lblSubTotal;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JTable tblCompraDetalle;
    private javax.swing.JTextField txtNroCompra;
    private javax.swing.JTextField txtNroDoc;
    private com.toedter.calendar.JDateChooser txtNroFecha;
    private javax.swing.JTextField txtNroSerie;
    private javax.swing.JTextField txtPorIGV;
    private javax.swing.JTextField txtRUC;
    // End of variables declaration//GEN-END:variables
}
