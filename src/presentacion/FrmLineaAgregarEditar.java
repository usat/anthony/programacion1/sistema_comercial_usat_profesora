package presentacion;

import java.sql.ResultSet;
import logica.Linea;
import util.Funciones;

public class FrmLineaAgregarEditar extends javax.swing.JDialog {

    public String Operacion;
    public String grabarCorrectamente;

    public FrmLineaAgregarEditar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.txtCodigo.setEditable(false);
        this.txtNombre.requestFocus();
        this.grabarCorrectamente = "no";
    }

    public void leerDatos(int codigoArticulo) {
        try {
            Linea objLinea = new Linea();
            ResultSet resultado = objLinea.leerDatos(codigoArticulo);
            if (resultado.next()) {
                this.txtCodigo.setText(resultado.getString("Codigo_linea"));
                this.txtNombre.setText(resultado.getString("descripcion"));
            }
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGuardar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnSalir.setText("Salir");

        jLabel1.setText("Codigo:");

        jLabel2.setText("Nombre:");

        txtCodigo.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(btnGuardar)
                        .addGap(68, 68, 68)
                        .addComponent(btnSalir))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnSalir))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (txtNombre.getText().isEmpty()) {
            Funciones.mensajeAdvertencia("Debe ingresar el nombre del artículo", "Verifique");
            txtNombre.requestFocus();
            return;//Detener la operacion
        }

        String nombre = txtNombre.getText();

        Linea objLinea = new Linea();
        objLinea.setDescripcion(nombre);

        boolean resultado;

        if (Operacion.equalsIgnoreCase("agregar")) {
            try {
                int respuesta = Funciones.mensajeConfirmacion("Esta seguro de agregar el nuevo Línea", "Confirme");
                if (respuesta == 1) {//Hizo clic en el boton NO
                    return;//Detiene la ejecucion del programa
                }

                resultado = objLinea.agregar();
                if (resultado) {
                    //Si se ha grabado correctamente el nuevo articulo
                    grabarCorrectamente = "si";
                    this.dispose();
                }
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error al agregar");
            }
        } else {
            try {
                int codigoLinea = Integer.parseInt(this.txtCodigo.getText());
                
                int respuesta = Funciones.mensajeConfirmacion("¿Esta Seguro de editar los datos de linea?", "Confirme");
                if (respuesta == 1) {//Hizo clic en el boton NO
                    return;
                }
                objLinea.setCodigoLinea(codigoLinea);
                resultado = objLinea.editar();
                if (resultado) {
                    //Si se ha grabado correctamente el nuevo artículo
                    grabarCorrectamente = "si";
                    this.dispose();
                }
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error al actualizar");
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
