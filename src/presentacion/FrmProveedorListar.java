package presentacion;

import java.awt.Dimension;
import java.sql.ResultSet;
import javax.swing.ListSelectionModel;
import logica.Proveedor;
import util.Funciones;

public class FrmProveedorListar extends javax.swing.JInternalFrame {

    ResultSet resultado;

    public FrmProveedorListar() {
        initComponents();
        // Redimenzionar dimensiones de formulario
        this.setSize(Funciones.ANCHO_CONTENEDOR, Funciones.ALTO_CONTENEDOR);

        // Limpiar y llenar combo cboCampoFiltro
        this.configurarColumnasFiltro();
        this.cboCampoFiltro.setSelectedIndex(0);

        // Listar tabla
        this.obtenerResultados();
        this.listar();

        this.tblListado.setRowSelectionInterval(0, 0);
        this.tblListado.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    }

    private void configurarColumnasFiltro() {
        this.cboCampoFiltro.removeAllItems();
        this.cboCampoFiltro.addItem("ruc");
        this.cboCampoFiltro.addItem("razon_social");
    }

    private void obtenerResultados()//Actualiza
    {
        try {
            this.resultado = new Proveedor().listar();
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    private void listar()//Muestra
    {
        try {
            String alineacionColumnas[] = {"C", "I", "I", "I", "I"};
            int anchoColumnas[] = {80, 350, 350, 350, 350};

            String campoFiltro = "";
            if (this.cboCampoFiltro.getSelectedIndex() >= 0) {
                campoFiltro = this.cboCampoFiltro.getSelectedItem().toString();
            } else {
                campoFiltro = "ruc"; //Nombre del artículo
            }
            String valorBusqueda = txtValorBusqueda.getText();
            Funciones.llenarTablaBusqueda(tblListado, resultado,
                    anchoColumnas, alineacionColumnas, campoFiltro, valorBusqueda);
            this.tblListado.setDefaultEditor(Object.class, null);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbOpciones = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        cboCampoFiltro = new javax.swing.JComboBox<String>();
        txtValorBusqueda = new javax.swing.JTextField();
        btnAgregar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblListado = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Listado de artìculos");
        addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                formFocusGained(evt);
            }
        });
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        tbOpciones.setFloatable(false);
        tbOpciones.setRollover(true);

        jLabel1.setText("Filtrar por:");
        tbOpciones.add(jLabel1);

        cboCampoFiltro.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboCampoFiltro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCampoFiltroActionPerformed(evt);
            }
        });
        tbOpciones.add(cboCampoFiltro);

        txtValorBusqueda.setPreferredSize(new java.awt.Dimension(500, 23));
        txtValorBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtValorBusquedaActionPerformed(evt);
            }
        });
        txtValorBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtValorBusquedaKeyReleased(evt);
            }
        });
        tbOpciones.add(txtValorBusqueda);

        btnAgregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add2.png"))); // NOI18N
        btnAgregar.setText("Agregar");
        btnAgregar.setFocusable(false);
        btnAgregar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAgregar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });
        tbOpciones.add(btnAgregar);

        btnEditar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/edit.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setFocusable(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        tbOpciones.add(btnEditar);

        btnEliminar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/delete.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        tbOpciones.add(btnEliminar);

        jLabel2.setText("            ");
        tbOpciones.add(jLabel2);

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir2.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        tbOpciones.add(btnSalir);

        tblListado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblListado);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tbOpciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tbOpciones, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtValorBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorBusquedaKeyReleased
        // TODO add your handling code here:
        this.listar();
    }//GEN-LAST:event_txtValorBusquedaKeyReleased

    private void cboCampoFiltroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCampoFiltroActionPerformed
        this.txtValorBusqueda.setText("");
        this.txtValorBusqueda.requestFocus();
        this.obtenerResultados();
        this.listar();
    }//GEN-LAST:event_cboCampoFiltroActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        FrmProveedoresAgregarEditar objFAE = new FrmProveedoresAgregarEditar(null, true);
        objFAE.setTitle("Agregar nuevoo Proveedor");
        objFAE.Operacion = "agregar";
        objFAE.setVisible(true);

        if (objFAE.grabarCorrectamente.equalsIgnoreCase("si")) {
            this.obtenerResultados();
            this.listar();
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        FrmProveedoresAgregarEditar objFAE = new FrmProveedoresAgregarEditar(null, true);

        objFAE.setTitle("Editar datos de Proveedor");
        objFAE.Operacion = "Editar";

        /*Capturar el código del artículo para leer sus datos*/
        int filaSeleccionada = this.tblListado.getSelectedRow();
        if (filaSeleccionada < 0) {
            Funciones.mensajeError("Debe seleccionar una fila", "Verifique");
            return;//Detiene la app
        }
        /*Capturar el codigo*/
        String rucProveedor = String.valueOf(this.tblListado.getValueAt(filaSeleccionada, 0).toString());
        //System.out.println("COD SEL:" + codigoArticulo);
        /*Capturar el codigo*/
        objFAE.leerDatos(rucProveedor);
        /*Capturar el codigo del articulo para leer sus datos*/
        objFAE.setVisible(true);

        if (objFAE.grabarCorrectamente.equalsIgnoreCase("si")) {
            this.obtenerResultados();
            this.listar();
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        /*Capturar el código del árticulo para eliminar*/
        int filaSeleccionada = this.tblListado.getSelectedRow();
        if (filaSeleccionada < 0) {
            Funciones.mensajeError("Debe seleccionar una fila", "Verifique");
            return;//Detiene la app
        }
        String rucProveedor = String.valueOf(this.tblListado.getValueAt(filaSeleccionada, 0).toString());

        int r = Funciones.mensajeConfirmacion("Esta seguro de eliminar la Línea seleccionado", "Confirme");

        if (r == 0) {
            try {
                Proveedor objArti = new Proveedor();
                boolean resultado = objArti.eliminar(rucProveedor);
                if (resultado) {
                    this.obtenerResultados();
                    this.listar();
                }
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error al eliminar");
            }
        }

    }//GEN-LAST:event_btnEliminarActionPerformed

    private void txtValorBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtValorBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtValorBusquedaActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void formFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_formFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_formFocusGained

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        // TODO add your handling code here:
        this.cboCampoFiltro.setPreferredSize(new Dimension(180, 25));
        this.txtValorBusqueda.setPreferredSize(new Dimension(250, 25));

        this.tbOpciones.add(this.cboCampoFiltro, 1);
        this.tbOpciones.add(this.txtValorBusqueda, 2);

        this.txtValorBusqueda.requestFocus();
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cboCampoFiltro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar tbOpciones;
    private javax.swing.JTable tblListado;
    private javax.swing.JTextField txtValorBusqueda;
    // End of variables declaration//GEN-END:variables
}
