package presentacion;

import java.sql.Date;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import logica.Compra;
import util.Funciones;

public class FrmCompraRegistro extends javax.swing.JInternalFrame {

    public FrmCompraRegistro() {
        initComponents();

        // Redimenzionar dimensiones de formulario
        this.setSize(Funciones.ANCHO_CONTENEDOR, Funciones.ALTO_CONTENEDOR);
        this.cargarFechaActual();
        /*Muestre el primer radioButton seleccionado*/
        this.rbTodasLasFechas.setSelected(true);
        /*Desactivar las cajas de texto DESDE y HASTA*/
        this.txtDesde.setEnabled(false);
        this.txtHasta.setEnabled(false);
        /*Desactivar las cajas de texto DESDE y HASTA*/
        /*Hace click automáticamente en el balón FILTRAR al ingresar al formulario*/
        this.btnFiltrar.doClick();
    }

    private void cargarFechaActual() {
        java.util.Date fecha = new java.util.Date();
        this.txtDesde.setDate(fecha);
        this.txtHasta.setDate(fecha);
    }

    public void listar() {

        int tipo = 0;
        if (this.rbHoy.isSelected()) {
            tipo = 1;
        } else if (this.rbPorFecha.isSelected()) {
            tipo = 2;
        } else {
            tipo = 3;
        }

        java.sql.Date fecha1 = new Date(this.txtDesde.getDate().getTime());
        java.sql.Date fecha2 = new Date(this.txtHasta.getDate().getTime());

        try {
            Compra objCompra = new Compra();
            ResultSet resultado = objCompra.listar(tipo, fecha1, fecha2);

            int anchoCol[] = {70, 70, 80, 100, 95, 220, 110, 90, 80, 80, 80, 80};
            String alineacionCol[] = {"I", "C", "C", "C", "I", "I", "C", "D", "D", "D", "D", "C"};

            Funciones.llenarTabla(tblListado, resultado, anchoCol, alineacionCol);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        };
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        rbHoy = new javax.swing.JRadioButton();
        rbPorFecha = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtDesde = new com.toedter.calendar.JDateChooser();
        txtHasta = new com.toedter.calendar.JDateChooser();
        rbTodasLasFechas = new javax.swing.JRadioButton();
        btnFiltrar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblListado = new javax.swing.JTable();
        btnAgregar = new javax.swing.JButton();
        btnAnular = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/compra.png"))); // NOI18N
        jLabel1.setText("Registro de Compras");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtrar por fecha"));
        jPanel2.setName(""); // NOI18N

        rbGroup.add(rbHoy);
        rbHoy.setText("Hoy");
        rbHoy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbHoyActionPerformed(evt);
            }
        });

        rbGroup.add(rbPorFecha);
        rbPorFecha.setText("Por fecha");
        rbPorFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPorFechaActionPerformed(evt);
            }
        });

        jLabel3.setText("Desde:");

        jLabel4.setText("Hasta:");

        rbGroup.add(rbTodasLasFechas);
        rbTodasLasFechas.setText("Todas las fechas");
        rbTodasLasFechas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTodasLasFechasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbTodasLasFechas)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(rbPorFecha)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDesde, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtHasta, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(rbHoy))
                .addContainerGap(83, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtDesde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(rbHoy)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbPorFecha)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)))
                    .addComponent(txtHasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbTodasLasFechas)
                .addContainerGap())
        );

        btnFiltrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/transaccion.png"))); // NOI18N
        btnFiltrar.setText("Filtrar");
        btnFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFiltrarActionPerformed(evt);
            }
        });

        tblListado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblListado);

        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add2.png"))); // NOI18N
        btnAgregar.setText("Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnAnular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/anular.png"))); // NOI18N
        btnAnular.setText("Anular");
        btnAnular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnularActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnSalir.setText("Salr");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnAgregar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAnular)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnSalir))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnFiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 76, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(btnFiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregar)
                    .addComponent(btnAnular)
                    .addComponent(btnSalir))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbHoyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbHoyActionPerformed
        /*Descativar las cajas de texto DESDE y HASTA*/
        this.txtDesde.setEnabled(false);
        this.txtHasta.setEnabled(false);
        /*Desactivar las cajas de texto DESDE y HASTA*/
    }//GEN-LAST:event_rbHoyActionPerformed

    private void rbPorFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPorFechaActionPerformed
        /*Activar las cajas de texto DESDE y HASTA*/
        this.txtDesde.setEnabled(true);
        this.txtHasta.setEnabled(true);
        this.txtDesde.requestFocus();
        /*Activar las cajas de texto DESDE y HASTA*/
    }//GEN-LAST:event_rbPorFechaActionPerformed

    private void rbTodasLasFechasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTodasLasFechasActionPerformed
        /*Descativar las cajas de texto DESDE y HASTA*/
        this.txtDesde.setEnabled(false);
        this.txtHasta.setEnabled(false);
        /*Desactivar las cajas de texto DESDE y HASTA*/
    }//GEN-LAST:event_rbTodasLasFechasActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        FrmCompra objCompra = new FrmCompra(null, true);
        objCompra.setTitle("Resgistrar nueva Compra");
        objCompra.setVisible(true);

        /*Preguntar el formulario FrmCompra grabo correctamente*/
        if (objCompra.accion == 1) {
            this.rbHoy.setSelected(true);
            this.btnFiltrar.doClick();
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFiltrarActionPerformed
        listar();
    }//GEN-LAST:event_btnFiltrarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnAnularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnularActionPerformed
        int fila = tblListado.getSelectedRow();
        if (fila < 0) {
            Funciones.mensajeError("No ha seleccionado un registro", "Sofware Anthony");
            return;
        }

        /**
         * CAPTURA DATO*
         */
        int codigoArticulo = Integer.parseInt(tblListado.getValueAt(fila, 0).toString());
        /**
         * CAPTURA DATO*
         */

//        JOptionPane.showMessageDialog(this, tblListado.getValueAt(fila, 0).toString());
        int r = Funciones.mensajeConfirmacion("¿Estás seguro de anular el registro seleccionado?", "Confirme...");

        if (r != 0) {
            return;
        }
        try {
            boolean resultado;
            Compra obj = new Compra();
            resultado = obj.anularCompra(codigoArticulo);
            if (resultado) {
                this.listar();
            }else{
                JOptionPane.showMessageDialog(this, "Ya está anulado");
            }
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(),"Sofware Anthony");
        }
    }//GEN-LAST:event_btnAnularActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnAnular;
    private javax.swing.JButton btnFiltrar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.ButtonGroup rbGroup;
    private javax.swing.JRadioButton rbHoy;
    private javax.swing.JRadioButton rbPorFecha;
    private javax.swing.JRadioButton rbTodasLasFechas;
    private javax.swing.JTable tblListado;
    private com.toedter.calendar.JDateChooser txtDesde;
    private com.toedter.calendar.JDateChooser txtHasta;
    // End of variables declaration//GEN-END:variables
}
