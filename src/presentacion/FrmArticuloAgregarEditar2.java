package presentacion;

import java.sql.ResultSet;
import logica.Articulo;
import logica.Categoria;
import logica.Linea;
import logica.Marca;
import util.Funciones;

public class FrmArticuloAgregarEditar2 extends javax.swing.JDialog {

    public String Operacion;
    public String grabarCorrectamente;

    public FrmArticuloAgregarEditar2(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        //Desactivar la caja de txto "txtPorcentajeDescuento"
        this.txtPorcentajeDescuento.setEnabled(false);
        this.txtPorcentajeDescuento.setText("0");
        //Seleccionar el radio button "rbProducto"
        this.rbProducto.setSelected(true);
        //Colocar la caja de texto txtCodigo como No editable
        this.txtCodigo.setEditable(false);
        this.txtNombre.requestFocus();
        //Llamar al método que llena el combo de lieneas
        this.llenarComboLinea();
        //Llamar al método que llena el combo de Marca
        this.llenarComboMarca();
        //Inicializar la variable grabadoCorrectamente
        this.grabarCorrectamente = "no";

    }

    private void llenarComboLinea() {
        try {
            Linea objLista = new Linea();
            objLista.llenarComboLinea(cboLinea);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    private void llenarComboMarca() {
        try {
            Marca ObjMarca = new Marca();
            ObjMarca.llenarComboMarca(cboMarca);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }
    }

    public void leerDatos(int codigoArticulo) {
        try {
            Articulo objArticulo = new Articulo();
            ResultSet resultado = objArticulo.leerDatos(codigoArticulo);
            if (resultado.next()) {
                this.txtCodigo.setText(String.valueOf(resultado.getInt("Codigo")));
                this.txtNombre.setText(resultado.getString("nombre"));
                this.txtPrecioVenta.setText(String.valueOf(resultado.getDouble("precio_venta")));
                this.cboLinea.setSelectedItem(resultado.getString("Linea"));
                this.cboCategoria.setSelectedItem(resultado.getString("Categoria"));
                this.cboMarca.setSelectedItem(resultado.getString("marca"));
                String tipoArticulo = resultado.getString("tipo_articulo");

                if (tipoArticulo.equalsIgnoreCase("P")) {
                    this.rbProducto.setSelected(true);
                } else {
                    if (tipoArticulo.equalsIgnoreCase("I")) {
                        this.rbRepuesto.setSelected(true);
                    } else {
                        this.rbRepuesto.setSelected(true);
                    }
                }
                String tieneDescuento = resultado.getString("Tiene Descuento");
                if (tieneDescuento.equalsIgnoreCase("S")) {
                    this.chkTieneDescuento.setSelected(true);
                    this.txtPorcentajeDescuento.setEnabled(true);
                } else {
                    this.chkTieneDescuento.setSelected(false);
                    this.txtPorcentajeDescuento.setEnabled(false);
                }
                this.txtPorcentajeDescuento.setText(String.valueOf(resultado.getDouble("porcentaje_descuento")));
            }

        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPrecioVenta = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cboLinea = new javax.swing.JComboBox<String>();
        jLabel5 = new javax.swing.JLabel();
        cboCategoria = new javax.swing.JComboBox<String>();
        jLabel6 = new javax.swing.JLabel();
        cboMarca = new javax.swing.JComboBox<String>();
        jLabel7 = new javax.swing.JLabel();
        rbProducto = new javax.swing.JRadioButton();
        rbInsumo = new javax.swing.JRadioButton();
        rbRepuesto = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        txtStock = new javax.swing.JTextField();
        chkTieneDescuento = new javax.swing.JCheckBox();
        jLabel8 = new javax.swing.JLabel();
        txtPorcentajeDescuento = new javax.swing.JTextField();
        btnSalir = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Código:");

        txtCodigo.setEditable(false);

        jLabel2.setText("Nombre:");

        jLabel3.setText("Precio de venta:");

        txtPrecioVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPrecioVentaKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioVentaKeyTyped(evt);
            }
        });

        jLabel4.setText("Línea:");

        cboLinea.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboLinea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboLineaActionPerformed(evt);
            }
        });

        jLabel5.setText("Categoría:");

        cboCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setText("Marca:");

        cboMarca.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboMarca.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cboMarcaFocusLost(evt);
            }
        });

        jLabel7.setText("Tipo de artículo:");

        rbProducto.setSelected(true);
        rbProducto.setText("Producto");

        rbInsumo.setText("Insumo");

        rbRepuesto.setText("Repuesto");

        jLabel9.setText("Stock:");

        chkTieneDescuento.setText("Tiene descuento");
        chkTieneDescuento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkTieneDescuentoActionPerformed(evt);
            }
        });

        jLabel8.setText("Porcentaje descuento:");

        txtPorcentajeDescuento.setText("0.00");
        txtPorcentajeDescuento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPorcentajeDescuentoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPorcentajeDescuentoFocusLost(evt);
            }
        });
        txtPorcentajeDescuento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPorcentajeDescuentoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPorcentajeDescuentoKeyTyped(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtNombre)
                    .addComponent(txtPrecioVenta)
                    .addComponent(txtCodigo)
                    .addComponent(cboLinea, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboCategoria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboMarca, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(rbProducto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbInsumo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbRepuesto))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnGuardar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnSalir))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(chkTieneDescuento)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel8)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPorcentajeDescuento, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE))
                    .addComponent(txtStock))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtPrecioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboLinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cboMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(rbProducto)
                    .addComponent(rbInsumo)
                    .addComponent(rbRepuesto))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkTieneDescuento)
                    .addComponent(jLabel8)
                    .addComponent(txtPorcentajeDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnSalir))
                .addGap(30, 30, 30))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboLineaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboLineaActionPerformed
        try {
            if (this.cboLinea.getSelectedIndex() >= 0) {
                int codigoLinea = Linea.listarLineas.get(this.cboLinea.getSelectedIndex()).getCodigoLinea();
                Categoria objCat = new Categoria();
                objCat.llenarComboCategoria(cboCategoria, codigoLinea);
            }
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "Error");

        }
    }//GEN-LAST:event_cboLineaActionPerformed

    private void chkTieneDescuentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkTieneDescuentoActionPerformed
        if (chkTieneDescuento.isSelected()) {
            this.txtPorcentajeDescuento.setEnabled(true);
            this.txtPorcentajeDescuento.requestFocus();
            this.txtPorcentajeDescuento.setText("");
        } else {
            this.txtPorcentajeDescuento.setEnabled(false);
            this.txtPorcentajeDescuento.setText("0");

        }
    }//GEN-LAST:event_chkTieneDescuentoActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        /*Captura Datos*/
        if (txtNombre.getText().isEmpty()) {
            Funciones.mensajeAdvertencia("Debe ingresar el nombre del artículo", "Verifique");
            txtNombre.requestFocus();
            return;//Detener la operacion
        } else {
            if (txtPrecioVenta.getText().isEmpty()) {// Si está vacio (is empy)
                Funciones.mensajeAdvertencia("Debe ingresar el precio del articulo", "Vrifique");
                txtPrecioVenta.requestFocus();
                return;
            } else {
                if (txtPorcentajeDescuento.getText().isEmpty()) {
                    Funciones.mensajeAdvertencia("Debe ingresar el porcentaje de descuento", "Verifique");
                    txtPorcentajeDescuento.requestFocus();
                    return;
                }
            }
        }

        /*Captura de datos*/
        String nombre = txtNombre.getText();
        double precioVenta = Double.parseDouble(txtPrecioVenta.getText());
        int codigoCategoria = Categoria.listarCategorias.get(cboCategoria.getSelectedIndex()).getCodigoCategoria();
        int codigoMarca = Marca.listarMarcas.get(cboMarca.getSelectedIndex()).getCodigoMarca();
        String tipoArticulo = "";
        if (rbProducto.isSelected()) {
            tipoArticulo = "P";//Producto
        } else {
            if (rbInsumo.isSelected()) {
                tipoArticulo = "I";//Insumo
            } else {
                tipoArticulo = "R";//Respuesto
            }
        }
        String tieneDescuento = "";
        if (chkTieneDescuento.isSelected()) {
            tieneDescuento = "S";
        } else {
            tieneDescuento = "N";
        }
        double porcentajeDescuento = Double.parseDouble(txtPorcentajeDescuento.getText());

        Articulo objArticulo = new Articulo();
        objArticulo.setNombre(nombre);
        objArticulo.setPrecioVenta(precioVenta);
        objArticulo.setCodigoCategoria(codigoCategoria);
        objArticulo.setCodigoMarca(codigoMarca);
        objArticulo.setTipoArticulo(tipoArticulo);
        objArticulo.setTieneDescuento(tieneDescuento);
        objArticulo.setPorcentajeDescuento(porcentajeDescuento);

        boolean resultado;

        if (Operacion.equalsIgnoreCase("agregar")) {
            try {
                int respuesta = Funciones.mensajeConfirmacion("Esta seguro de agregar el nuevo artículo", "Confirme");
                if (respuesta == 1) {//Hizo clic en el boton NO
                    return;//Detiene la ejecucion del programa
                }

                resultado = objArticulo.agregar();
                if (resultado) {
                    //Si se ha grabado correctamente el nuevo articulo
                    grabarCorrectamente = "si";
                    this.dispose();
                }
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error al agregar");
            }
        } else {
            try {
                int codigoArticulo = Integer.parseInt(this.txtCodigo.getText());
                int respuesta = Funciones.mensajeConfirmacion("¿Esta Seguro de editar los datos del artículo?", "Confirme");
                if (respuesta == 1) {//Hizo clic en el boton NO
                    return;
                }
                objArticulo.setCodigoArticulo(codigoArticulo);
                resultado = objArticulo.editar();
                if (resultado) {
                    //Si se ha grabado correctamente el nuevo artículo
                    grabarCorrectamente = "si";
                    this.dispose();
                }
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error al actualizar");
            }
        }


    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtPrecioVentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioVentaKeyPressed

    }//GEN-LAST:event_txtPrecioVentaKeyPressed

    private void txtPorcentajeDescuentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPorcentajeDescuentoKeyReleased

    }//GEN-LAST:event_txtPorcentajeDescuentoKeyReleased

    private void txtPorcentajeDescuentoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPorcentajeDescuentoFocusGained

    }//GEN-LAST:event_txtPorcentajeDescuentoFocusGained

    private void cboMarcaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cboMarcaFocusLost

    }//GEN-LAST:event_cboMarcaFocusLost

    private void txtPorcentajeDescuentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPorcentajeDescuentoKeyTyped
        Funciones.soloNumerosDecimal(evt, txtPorcentajeDescuento, 5);
    }//GEN-LAST:event_txtPorcentajeDescuentoKeyTyped

    private void txtPrecioVentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioVentaKeyTyped
        Funciones.soloNumerosDecimal(evt, txtPrecioVenta, 10);
    }//GEN-LAST:event_txtPrecioVentaKeyTyped

    private void txtPorcentajeDescuentoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPorcentajeDescuentoFocusLost
        //
        double pd = Double.parseDouble(txtPorcentajeDescuento.getText());
        if (pd < 0 || pd > 100) {
            Funciones.mensajeAdvertencia("Ingrese un porcentae de descuento entre 0 y 100", "Verifique");
            txtPorcentajeDescuento.setText("0");
            txtPorcentajeDescuento.requestFocus();
        }
    }//GEN-LAST:event_txtPorcentajeDescuentoFocusLost

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cboCategoria;
    private javax.swing.JComboBox<String> cboLinea;
    private javax.swing.JComboBox<String> cboMarca;
    private javax.swing.JCheckBox chkTieneDescuento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JRadioButton rbInsumo;
    private javax.swing.JRadioButton rbProducto;
    private javax.swing.JRadioButton rbRepuesto;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPorcentajeDescuento;
    private javax.swing.JTextField txtPrecioVenta;
    private javax.swing.JTextField txtStock;
    // End of variables declaration//GEN-END:variables
}
