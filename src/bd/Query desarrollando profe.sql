﻿Select * from fn_anular(2)

CREATE OR REPLACE FUNCTION fn_anular(p_idCompra int)
  RETURNS boolean AS
$BODY$
	Declare
		v_Retornar boolean;
		v_compra_cursor refcursor;
		v_compra_registro record;
		v_compra_validacion varchar;
	begin
		v_Retornar= false;

		--0) validación
		Select estado into v_compra_validacion from compra where numero_compra =p_idCompra;
		IF(v_compra_validacion = 'A') then
			return v_Retornar;
		END IF;

		--1) Cambiar de estado
		UPDATE public.compra
		   SET estado='A'
		 WHERE numero_compra=p_idCompra;

		--2)Restar del inventario

			--2.1 capturando datos
				open v_compra_cursor for 
					select codigo_articulo,cantidad
					from compra_detalle
					where numero_compra = p_idCompra;

			--2.2 restar en los articulos
				LOOP
					fetch v_compra_cursor into v_compra_registro;
					if FOUND then
						UPDATE articulo
						   SET stock=stock-v_compra_registro.cantidad
						 WHERE codigo_articulo=v_compra_registro.codigo_articulo;
						 
						 v_Retornar= true;
					else
						--si no encuentra, salir del bucle
						exit;
					end if;
				END LOOP;

				return v_Retornar;
	end
	
$BODY$
  LANGUAGE plpgsql VOLATILE;

  Select * from compra
  Select * from compra_detalle where numero_compra=6
	
  Select * from articulo where codigo_articulo = 8

  SELECT 
  compra_detalle.numero_compra, 
  articulo.codigo_articulo,
  articulo.nombre, 
  articulo.precio_venta, 
  articulo.stock, 
  compra_detalle.item, 
  compra_detalle.cantidad, 
  compra_detalle.precio, 
  compra_detalle.descuento, 
  compra_detalle.importe, 
  compra.estado
FROM 
  public.compra, 
  public.compra_detalle, 
  public.articulo
WHERE 
  compra.numero_compra = compra_detalle.numero_compra AND
  compra_detalle.codigo_articulo = articulo.codigo_articulo
  --order by 1
   and
  compra_detalle.numero_compra=6;

  Select * from articulo = 