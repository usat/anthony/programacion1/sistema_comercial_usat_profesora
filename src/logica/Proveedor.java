package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Proveedor extends Conexion {

    String ruc_proveedor, razon_social, direccion, telefono, representante_legal;

    public String getRuc_proveedor() {
        return ruc_proveedor;
    }

    public void setRuc_proveedor(String ruc_proveedor) {
        this.ruc_proveedor = ruc_proveedor;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRepresentante_legal() {
        return representante_legal;
    }

    public void setRepresentante_legal(String representante_legal) {
        this.representante_legal = representante_legal;
    }

    public ResultSet listar() throws Exception {
        String sql = "Select ruc_proveedor as ruc,"
                + "razon_social, "
                + "direccion, "
                + "telefono,"
                + "representante_legal "
                + "from proveedor "
                + "order by razon_social";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean editar() throws Exception {
        /*Configurar la transacción*/
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        /*1. Primera Operacion*/
        String sql = "UPDATE public.proveedor\n"
                + "   SET  razon_social=?, direccion=?, telefono=?, representante_legal=?\n"
                + " WHERE ruc_proveedor=?;";
        PreparedStatement spActualizar = transaccion.prepareStatement(sql);

        spActualizar.setString(5, this.getRuc_proveedor());
        spActualizar.setString(1, this.getRazon_social());
        spActualizar.setString(2, this.getDireccion());
        spActualizar.setString(3, this.getTelefono());
        spActualizar.setString(4, this.getRepresentante_legal());

        this.ejecutarSQL(spActualizar, transaccion);

        /*Confirmar y terminar la transacción*/
        transaccion.commit();
        transaccion.close();
        return true;
    }

    public ResultSet leerDatos(String RucProveedor) throws Exception {
        String sql = "  select * from proveedor where ruc_proveedor= ?";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setString(1, RucProveedor);
        ResultSet resultado = this.ejecutarSQL(sp);
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = " INSERT INTO public.proveedor(\n"
                    + "            ruc_proveedor, razon_social, direccion, telefono, representante_legal)\n"
                    + "    VALUES (?, ?, ?, ?, ?);";
            PreparedStatement sp = transaccion.prepareStatement(sql);
            sp.setString(1, this.getRuc_proveedor());
            sp.setString(2, this.getRazon_social());
            sp.setString(3, this.getDireccion());
            sp.setString(4, this.getTelefono());
            sp.setString(5, this.getRepresentante_legal());
            this.ejecutarSQL(sp, transaccion);

            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public boolean eliminar(String RucProveedoro) throws Exception {
        /*Configurar la transaccion*/
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);
        /*Configurar la transaccion*/

        String sql = "delete from proveedor where ruc_proveedor =?";
        PreparedStatement spEliminar = transaccion.prepareStatement(sql);
        spEliminar.setString(1, RucProveedoro);
        this.ejecutarSQL(spEliminar, transaccion);

        //Confirmar y cerrar la transaccion
        transaccion.commit();
        transaccion.close();
        return true;
    }

}
