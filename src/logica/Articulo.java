package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Articulo extends Conexion {

    private int codigoArticulo;
    private String nombre;
    private double precioVenta;
    private int codigoCategoria;
    private int codigoMarca;
    private int stock;
    private String tipoArticulo;
    private String tieneDescuento;
    private double porcentajeDescuento;

    public int getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(int codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getCodigoCategoria() {
        return codigoCategoria;
    }

    public void setCodigoCategoria(int codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    public int getCodigoMarca() {
        return codigoMarca;
    }

    public void setCodigoMarca(int codigoMarca) {
        this.codigoMarca = codigoMarca;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getTieneDescuento() {
        return tieneDescuento;
    }

    public void setTieneDescuento(String tieneDescuento) {
        this.tieneDescuento = tieneDescuento;
    }

    public double getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(double porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public ResultSet listar() throws Exception {
        String sql = "  select"
                + "   a.codigo_articulo as codigo,"
                + "   a.nombre,"
                + "   a.precio_venta,"
                + "   a.stock,"
                + "   a.tipo_articulo,"
                + "   c.descripcion as categoria,"
                + "   l.descripcion as linea,"
                + "   m.descripcion as marca"
                + "   from articulo a "
                + "   inner join categoria c on (a.codigo_categoria=c.codigo_categoria)"
                + "   inner join linea l on (c.codigo_linea=l.codigo_linea)"
                + "   inner join marca m on (a.codigo_marca=m.codigo_marca)"
                + "order by a.nombre asc";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean editar() throws Exception {
        /*Configurar la transacción*/
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        /*1. Primera Operacion*/
        String sql = "UPDATE"
                + " articulo "
                + "SET nombre=?,"
                + "precio_venta=?,"
                + "codigo_categoria=?,"
                + "codigo_marca=?,"
                + "tipo_articulo=?,"
                + "tiene_descuento=?,"
                + "porcentaje_descuento=?"
                + "WHERE"
                + " codigo_articulo=?";
        PreparedStatement spActualizar = transaccion.prepareStatement(sql);

        spActualizar.setString(1, this.getNombre());
        spActualizar.setDouble(2, this.getPrecioVenta());
        spActualizar.setInt(3, this.getCodigoCategoria());
        spActualizar.setInt(4, this.getCodigoMarca());
        spActualizar.setString(5, this.getTipoArticulo());
        spActualizar.setString(6, this.getTieneDescuento());
        spActualizar.setDouble(7, this.getPorcentajeDescuento());
        spActualizar.setInt(8, this.getCodigoArticulo());

        this.ejecutarSQL(spActualizar, transaccion);

        /*Confirmar y terminar la transacción*/
        transaccion.commit();
        transaccion.close();
        return true;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "select * from fn_articulo_agregar(?,?,?,?,?,?,?,?)";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getNombre());
            spInsertar.setDouble(2, this.getPrecioVenta());
            spInsertar.setInt(3, this.getCodigoCategoria());
            spInsertar.setInt(4, this.getCodigoMarca());
            spInsertar.setInt(5, this.getStock());
            spInsertar.setString(6, this.getTipoArticulo());
            spInsertar.setString(7, this.getTieneDescuento());
            spInsertar.setDouble(8, this.getPorcentajeDescuento());
            ResultSet resultado = this.ejecutarSQL2(spInsertar);

            if (resultado.next()) {
                transaccion.commit();
            } else {
                transaccion.rollback();
            }

            transaccion.close();
            this.cerrarConexion(transaccion);

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public ResultSet leerDatos(int codigoArticulo) throws Exception {
        String sql = "  select"
                + "   a.codigo_articulo as codigo,"
                + "   a.nombre,"
                + "   a.precio_venta,"
                + "   a.tipo_articulo,"
                + "   c.descripcion as categoria,"
                + "   l.descripcion as linea,"
                + "   m.descripcion as marca,"
                + "   a.tiene_descuento,"
                + "   a.porcentaje_descuento"
                + "   from articulo a "
                + "   inner join categoria c on (a.codigo_categoria=c.codigo_categoria)"
                + "   inner join linea l on (c.codigo_linea=l.codigo_linea)"
                + "   inner join marca m on (a.codigo_marca=m.codigo_marca)"
                + "   WHERE"
                + "   a.codigo_articulo = ?";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setInt(1, codigoArticulo);
        ResultSet resultado = this.ejecutarSQL(sp);
        return resultado;
    }

    public boolean eliminar(int codigoArticulo) throws Exception {
        /*Configurar la transaccion*/
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);
        /*Configurar la transaccion*/

        String sql = "delete from articulo where codigo_articulo =?";
        PreparedStatement spEliminar = transaccion.prepareStatement(sql);
        spEliminar.setInt(1, codigoArticulo);
        this.ejecutarSQL(spEliminar, transaccion);

        //Confirmar y cerrar la transaccion
        transaccion.commit();
        transaccion.close();
        return true;
    }

    public ResultSet leerDatosCodigo(int cod_Art) throws Exception {
        String sql = "SELECT \n"
                + "  articulo.codigo_articulo, \n"
                + "  articulo.nombre, \n"
                + "  articulo.precio_venta, \n"
                + "  linea.codigo_linea, \n"
                + "  linea.descripcion as linea, \n"
                + "  categoria.codigo_categoria, \n"
                + "  categoria.descripcion as categoria, \n"
                + "  marca.codigo_marca, \n"
                + "  marca.descripcion as marca, \n"
                + "  articulo.stock, \n"
                + "  articulo.tipo_articulo, \n"
                + "  articulo.tiene_descuento, \n"
                + "  articulo.porcentaje_descuento\n"
                + "FROM \n"
                + "  public.articulo, \n"
                + "  public.categoria, \n"
                + "  public.linea, \n"
                + "  public.marca\n"
                + "WHERE \n"
                + "  categoria.codigo_linea = linea.codigo_linea AND\n"
                + "  categoria.codigo_categoria = articulo.codigo_categoria AND\n"
                + "  marca.codigo_marca = articulo.codigo_marca\n"
                + "  AND articulo.codigo_articulo = ?;";
        PreparedStatement sentencia = abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, cod_Art);
        return ejecutarSQL(sentencia);
    }

}
