package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Tipo_comprobante extends Conexion {

    private String codigoTC, descripcion;

    public static ArrayList<Tipo_comprobante> listarTC = new ArrayList<Tipo_comprobante>();

    public String getCodigoTC() {
        return codigoTC;
    }

    public void setCodigoTC(String codigoTC) {
        this.codigoTC = codigoTC;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private void cargarDatos() throws Exception {
        String sql = "select * from tipo_comprobante order by descripcion";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = this.ejecutarSQL(sentencia);
        //ResultSet resultado = sp.executeQuery();
        listarTC.clear();

        while (resultado.next()) {
            Tipo_comprobante objTC = new Tipo_comprobante();
            objTC.setCodigoTC(resultado.getString("codigo_tipo_comprobante"));
            objTC.setDescripcion(resultado.getString("Descripcion"));
            listarTC.add(objTC);
        }
    }

    public void llenarComboTC(JComboBox combo) throws Exception {
        combo.removeAllItems();
        this.cargarDatos();

        for (int i = 0; i < listarTC.size(); i++) {
            Tipo_comprobante m = listarTC.get(i);
            combo.addItem(m.getDescripcion());
        }
    }
}
