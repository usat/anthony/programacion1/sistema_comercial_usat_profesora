package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author laboratorio_computo
 */
public class Marca extends Conexion
{
    private int codigoMarca;
    private String descripcion;
    public static ArrayList<Marca> listarMarcas= new ArrayList<Marca>();

    public int getCodigoMarca() {
        return codigoMarca;
    }

    public void setCodigoMarca(int codigoMarca) {
        this.codigoMarca = codigoMarca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    private void cargarDatos() throws Exception
    {
        String sql = "select * from marca order by descripcion asc";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        listarMarcas.clear();
        while(resultado.next())
        {
            Marca objMarca = new Marca();
            objMarca.setCodigoMarca(resultado.getInt("codigo_marca"));
            objMarca.setDescripcion(resultado.getString("descripcion"));
            listarMarcas.add(objMarca);
        }
    }
    
    public void llenarComboMarca(JComboBox combo) throws Exception
    {
        this.cargarDatos();
        combo.removeAllItems();
        for(int i = 0; i<listarMarcas.size(); i++)
        {
            Marca m = listarMarcas.get(i);
            combo.addItem(m.getDescripcion());
        }
    }
    
    
    
    
}
