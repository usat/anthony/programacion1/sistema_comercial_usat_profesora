package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import util.Funciones;

public class inicioSesion extends Conexion {

    private String dni_usuario, password;

    public String getDni_usuario() {
        return dni_usuario;
    }

    public void setDni_usuario(String dni_usuario) {
        this.dni_usuario = dni_usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int iniciarSesionSP() throws Exception {

        String sql = "select * from fn_iniciosesion4(?, ?)";

        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setString(1, this.getDni_usuario());
        sentencia.setString(2, this.getPassword());

        ResultSet resultado = this.ejecutarSQLSelectSP(sentencia);

        if (resultado.next()) { //Encontró registros
            Funciones.NOMBRE_USUARIO_INICIO_SESION = resultado.getString("nombre");
            Funciones.CODIGO_USUARIO_INICIO_SESION = resultado.getInt("codigoUsuario");
            return resultado.getInt("estado");
        }
        return -2;
    }

}
