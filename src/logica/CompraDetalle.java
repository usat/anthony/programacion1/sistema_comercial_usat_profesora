package logica;

import datos.Conexion;
import java.sql.ResultSet;

public class CompraDetalle extends Conexion {

    private int codigo_articulo;
    private int cantidad;
    private double precio;
    private double descuento;
    private double importe;

    public int getCodigo_articulo() {
        return codigo_articulo;
    }

    public void setCodigo_articulo(int codigo_articulo) {
        this.codigo_articulo = codigo_articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public ResultSet configurarTablaDetalle() throws Exception {
        String sql = "Select * from "
                + "("
                + "Select "
                + "     0::integer as codigo, "
                + "     ''::character varying as articulo,"
                + "     0::integer as cantidad, "
                + "     0.00::numeric(14,2) as precio, "
                + "     0.00::numeric(14,2) as descuento, "
                + "     0.00::numeric(14,2) as importe "
                + ") as tb_temporal "
                + "where "
                + "tb_temporal.codigo<>0";
        return this.ejecutarSQL(sql);
    }

    public double calcularImporte(int cantidad, double preicio, double descuento) {
        double importeBruto, importeNeto, montoDescuento;
        importeBruto = cantidad * preicio;
        montoDescuento = importeBruto * (descuento / 100);
        importeNeto = importeBruto - montoDescuento;
        return importeNeto;
    }
}
