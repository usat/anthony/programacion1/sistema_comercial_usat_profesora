package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONArray;

public class Compra extends Conexion {

    //JsonArray-- Se relaciona con Java Scrip para tener una salida web
    private String codigoTipoComprobante;
    private String rucProveedor;
    private int numeroSerie;
    private int numeroDocumento;
    private java.sql.Date fechaCompra;
    private double porcentajeIGV;
    private double subTotal;
    private double igv;
    private double total;
    private int codigoUsuario;

    private ArrayList<CompraDetalle> detalleCompra = new ArrayList<CompraDetalle>();

    public String getCodigoTipoComprobante() {
        return codigoTipoComprobante;
    }

    public void setCodigoTipoComprobante(String codigoTipoComprobante) {
        this.codigoTipoComprobante = codigoTipoComprobante;
    }

    public String getRucProveedor() {
        return rucProveedor;
    }

    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    public int getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(int numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public double getPorcentajeIGV() {
        return porcentajeIGV;
    }

    public void setPorcentajeIGV(double porcentajeIGV) {
        this.porcentajeIGV = porcentajeIGV;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getIgv() {
        return igv;
    }

    public void setIgv(double igv) {
        this.igv = igv;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public ArrayList<CompraDetalle> getDetalleCompra() {
        return detalleCompra;
    }

    public void setDetalleCompra(ArrayList<CompraDetalle> detalleCompra) {
        this.detalleCompra = detalleCompra;
    }

    public ResultSet listar(int p_tipo, java.sql.Date p_fecha1, java.sql.Date p_fecha2) throws Exception {
        String sql = "select * from f_listar_compra(?,?,?)";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setInt(1, p_tipo);
        sp.setDate(2, p_fecha1);
        sp.setDate(3, p_fecha2);
        ResultSet resultado = this.ejecutarSQL(sp);
        return resultado;
    }

    public int registrarCompra() throws Exception {
        System.out.println("xx:" + this.getRucProveedor());
        String sql = "select f_registrar_compra"
                + "("
                + "     ?::character(2),"
                + "     ?::character(11),"
                + "     ?::integer,"
                + "     ?::integer,"
                + "     ?::date,"
                + "     ?::numeric,"
                + "     ?::numeric,"
                + "     ?::numeric,"
                + "     ?::numeric,"
                + "     ?::int,"
                + "     to_json(?::json)"/*mandando la conversion*/
                + ")    as retorno;";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setString(1, this.getCodigoTipoComprobante());
        sp.setString(2, this.getRucProveedor());
        sp.setInt(3, this.getNumeroSerie());
        sp.setInt(4, this.getNumeroDocumento());
        sp.setDate(5, this.getFechaCompra());
        sp.setDouble(6, this.getPorcentajeIGV());
        sp.setDouble(7, this.getSubTotal());
        sp.setDouble(8, this.getIgv());
        sp.setDouble(9, this.getTotal());
        sp.setInt(10, this.getCodigoUsuario());
        JSONArray jsonDetalleCompra = new JSONArray(detalleCompra);
        sp.setString(11, jsonDetalleCompra.toString());

        ResultSet resultado = this.ejecutarSQL(sp);

        if (resultado.next()) {
            return resultado.getInt("retorno");
        }
        return 0;

    }

    public boolean anularCompra(int codigoCompra) throws Exception {
        /*Configurar la transaccion*/
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);
        /*Configurar la transaccion*/

        String sql = "Select * from fn_anularcompra(?)";
        PreparedStatement spAnular = transaccion.prepareStatement(sql);
        spAnular.setInt(1, codigoCompra);
        ResultSet retor = this.ejecutarSQL2(spAnular);
        while (retor.next()) {
            if (retor.getBoolean(1) != true) {
                return false;
            }
        }

        //Confirmar y cerrar la transaccion
        transaccion.commit();
        transaccion.close();
        return true;
    }
}
