package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class configuracion extends Conexion {

    public String obtenerValorConfiguracion(int codigo) throws Exception {
        String sql = "Select valor from configuracion where codigo =?";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setInt(1, codigo);

        ResultSet resultado = this.ejecutarSQL(sp);
        if (resultado.next()) {
            return resultado.getString("valor");
        } else {
            throw new Exception("No se ha encontrado el código de configuración");
        }
    }
}
