package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Linea extends Conexion {

    private int codigoLinea;
    private String descripcion;
    public static ArrayList<Linea> listarLineas = new ArrayList<Linea>();

    public int getCodigoLinea() {
        return codigoLinea;
    }

    public void setCodigoLinea(int codigoLinea) {
        this.codigoLinea = codigoLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ResultSet listar() throws Exception {
        String sql = "  select * from linea";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "select fn_linea_agregar(?)";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getDescripcion());
            ResultSet resultado = this.ejecutarSQL2(spInsertar);

            if (resultado.next()) {
                transaccion.commit();
            } else {
                transaccion.rollback();
            }

            transaccion.close();
            this.cerrarConexion(transaccion);

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public boolean editar() throws Exception {
        /*Configurar la transacción*/
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        /*1. Primera Operacion*/
        String sql = "UPDATE public.linea"
                + "   SET descripcion=?"
                + " WHERE codigo_linea=?;";
        PreparedStatement spActualizar = transaccion.prepareStatement(sql);

        spActualizar.setString(1, this.getDescripcion());
        spActualizar.setInt(2, this.getCodigoLinea());

        this.ejecutarSQL(spActualizar, transaccion);

        /*Confirmar y terminar la transacción*/
        transaccion.commit();
        transaccion.close();
        return true;
    }

    public ResultSet leerDatos(int codigoLinea) throws Exception {
        String sql = " Select * from linea where codigo_linea = ?";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setInt(1, codigoLinea);
        ResultSet resultado = this.ejecutarSQL(sp);
        return resultado;
    }

    public boolean eliminar(int codigoLinea) throws Exception {
        /*Configurar la transaccion*/
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);
        /*Configurar la transaccion*/

        String sql = "delete from linea where codigo_linea =?";
        PreparedStatement spEliminar = transaccion.prepareStatement(sql);
        spEliminar.setInt(1, codigoLinea);
        this.ejecutarSQL(spEliminar, transaccion);

        //Confirmar y cerrar la transaccion
        transaccion.commit();
        transaccion.close();
        return true;
    }

    //---------------------------------------------------------------------------------------------------------------------
    private void cargarDatos() throws Exception {
        String sql = "select * from linea order by descripcion asc";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        listarLineas.clear();
        while (resultado.next()) {
            Linea objMarca = new Linea();
            objMarca.setCodigoLinea(resultado.getInt("codigo_linea"));
            objMarca.setDescripcion(resultado.getString("descripcion"));
            listarLineas.add(objMarca);
        }
    }

    public void llenarComboLinea(JComboBox combo) throws Exception {
        this.cargarDatos();
        combo.removeAllItems();
        for (int i = 0; i < listarLineas.size(); i++) {
            Linea m = listarLineas.get(i);
            combo.addItem(m.getDescripcion());
        }
    }

}
